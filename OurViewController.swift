//
//  OurViewController.swift
//  reminder2
//
//  Created by chenhsi on 2017/1/15.
//  Copyright © 2017年 chenhsi. All rights reserved.
//

import UIKit
import Social

class OurViewController: UIViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBAction func TwitterAction(_ sender: Any)
    {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter)
        {
            let twitterController = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
            twitterController?.setInitialText("I want to share my life.")
            self.present(twitterController!, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Account", message: "Please log in your Twitter", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler:
                {
                    (UIAlertAction) in
                    
                    let settingsURL = NSURL(string:UIApplicationOpenSettingsURLString)
                    
                    if let url = settingsURL
                    {
                        UIApplication.shared.openURL(url as URL)
                    }
                    
            }))
            self.present(alert, animated: true, completion:nil)
            
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil
        {
        menuButton.target = self.revealViewController()
        menuButton.action = "revealToggle:"
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
