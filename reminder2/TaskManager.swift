//
//  TaskManager.swift
//  reminder2
//
//  Created by chenhsi on 2016/7/7.
//  Copyright © 2016年 chenhsi. All rights reserved.
//

import UIKit
import CoreLocation
var taskMgr: TaskManager = TaskManager()

struct chose_place {
    var place: String
    var distance: Double
}

class TaskManager: NSObject {
    
    var tasks = [String]()
    var hours:Int = 0
    var times = [Date]()
    var maps = [CLLocation]()
    var types:String = ""
    var motivatations:Int = 0
    var importances:Int = 0
    var fixed_flexible:Bool = true  // true means fixed
    var happiness:Int = 0
    var urg:Int = 0
    var user_loading_vector = [0,0,0] // (Learn,Exercise,Hangout)
    typealias mytuple = (agr1:Date,agr2:CLLocation,agr3:String,agr4:Bool,agr5:String,agr6:Int,agr7:Int,agr8:Int,agr9:Int,agr10:Int) // time//venue//type//fixd or not// what is it // working hrs //motivation // importance // happiness //urgency
    var data_store = [mytuple]()
    var current_spot = CLLocation()
    var next_time = [Date]()
    var place_select = [chose_place]()
    var next_Imp:Int = 0
    var next_Mot:Int = 0
    var next_Hap:Int = 0
    var next_Urg:Int = 0
    var stored_text:String = ""
    var stored_mot:String = ""
    var day_left:String = ""
    var show_values = ["imp","urg","mot","happy","other"] //0:imp//1:urg//2:mot//3:happy//4:other
    var index:Int = 0
    override init() {
        
        super.init()
    }
    
    func addTask(_ event: String){ //what is it,
        tasks.append(event)
        }
    func addFinish_time(_ work_hour:String)  { // Working time
        print(work_hour)
        hours = Int(work_hour)!
    }
    func addTime(_ date:Date){ //time
        times.append(date)
        }
    func addVenue(_ venue:CLLocation)  { //venue
        maps.append(venue)
    }
    func addType(_ type:String) {//type
        types = type
    }
    func addMotivation(_ Moti: Int) {  //motivation
        motivatations = Moti
    }
    func addImportance(_ Imp: Int)  { //importance
        importances = Imp
    }
    func addFixed_or_Flexible(_ Flx: Bool)  {  //fixed or flexible
        fixed_flexible = Flx
    }
    func addHappiness(_ Hap: Int) {   //Happiness
        happiness = Hap
    }
    func addUrgency(_ Urgency: Int)  {   //Urgency
        urg = Urgency
    }
    func addPlace_distance(_ P:String, D:Double)  {   //Place+distance
        place_select.append(chose_place(place: P, distance: D))
    }
    }
