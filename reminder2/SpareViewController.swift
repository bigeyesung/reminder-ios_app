//
//  SpareViewController.swift
//  reminder2
//
//  Created by chenhsi on 2017/1/16.
//  Copyright © 2017年 chenhsi. All rights reserved.
//

import UIKit
import MapKit
class SpareViewController: UIViewController,CLLocationManagerDelegate
{
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var mapview: MKMapView!
    @IBOutlet weak var textview: UITextView!
    
    let radius = CLLocationDistance(500)
    var FFV = [Double]() //facility's feature vector
    var map_store = [MKCoordinateRegion]()
    var min_number:Double = 0.0
    var combined_text:String = ""
    var stored_text:String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Menu button
        if self.revealViewController() != nil
        {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        sparetime()
        textview.text = combined_text
    }
    
    func sparetime()
    {
        let learning_hour = taskMgr.user_loading_vector[0]
        
        
        if learning_hour < 3
        {
            // go to study
            let main_lib = CLLocation(latitude: 55.9426806,longitude:-3.1899106)
            var d = distanceFromLocation(main_lib)
            FFV.append(d)
            var center = CLLocationCoordinate2D(latitude: main_lib.coordinate.latitude, longitude: main_lib.coordinate.longitude)
            var region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[0]
            
            let elephant = CLLocation(latitude: 55.9476154,longitude: -3.1916038)
            d = distanceFromLocation(elephant)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: elephant.coordinate.latitude, longitude: elephant.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[1]
            
            
            let forest_hill = CLLocation(latitude:55.9460376,longitude:-3.1919055)
            d = distanceFromLocation(forest_hill)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: forest_hill.coordinate.latitude, longitude: forest_hill.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[2]
            
            
            let black_medicine = CLLocation(latitude:55.9470748,longitude:-3.1859786)
            d = distanceFromLocation(black_medicine)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: black_medicine.coordinate.latitude, longitude: black_medicine.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region)  //[3]
            
            
            let law = CLLocation(latitude: 55.9432781,longitude: -3.1861616)
            d = distanceFromLocation(law)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: law.coordinate.latitude, longitude: law.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region)  //[4]
            
            let moray = CLLocation(latitude: 55.9500858,longitude: -3.1831718)
            d = distanceFromLocation(moray)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: moray.coordinate.latitude, longitude: moray.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region)  //[5]
            
            
            let new = CLLocation(latitude: 55.9496908,longitude: -3.1970835)
            d = distanceFromLocation(new)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: new.coordinate.latitude, longitude: new.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region)  //[6]
            
            let noreen = CLLocation(latitude: 55.92295,longitude: -3.17504)
            d = distanceFromLocation(noreen)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: noreen.coordinate.latitude, longitude: noreen.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region)  //[7]
            
            
            let starbucks = CLLocation(latitude: 55.9439162,longitude:-3.1854949)
            d = distanceFromLocation(starbucks)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: starbucks.coordinate.latitude, longitude: starbucks.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region)  //[8]
            
            
            let kilomi_cafe = CLLocation(latitude: 55.9445328,longitude: -3.1857938)
            d = distanceFromLocation(kilomi_cafe)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: kilomi_cafe.coordinate.latitude, longitude: kilomi_cafe.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region)  //[9]
            min_number = FFV.min()!
            
            //vector V -- Judging elements
            if min_number == FFV[0]
            {
                stored_text = "The choice is Main library"
                self.mapview.setRegion(map_store[0], animated: true)
            }
            else if min_number == FFV[1]
            {
                stored_text = "The choice is The Elephant House"
                self.mapview.setRegion(map_store[1], animated: true)
            }
            else if min_number == FFV[2]
            {
                stored_text = "The choice is Forest Hill"
                self.mapview.setRegion(map_store[2], animated: true)
            }
            else if min_number == FFV[3]
            {
                stored_text = "The choice is Black Medicine"
                self.mapview.setRegion(map_store[3], animated: true)
            }
            else if min_number == FFV[4]
            {
                stored_text = "The choice is Law library"
                self.mapview.setRegion(map_store[4], animated: true)
            }
            else if min_number == FFV[5]
            {
                stored_text = "The choice is Moray House library"
                self.mapview.setRegion(map_store[5], animated: true)
            }
            else if min_number == FFV[6]
            {
                stored_text = "The choice is New College library"
                self.mapview.setRegion(map_store[6], animated: true)
            }
            else if min_number == FFV[7]
            {
                stored_text = "The choice is Noreen and Kenneth Murray library"
                self.mapview.setRegion(map_store[7], animated: true)
            }
            else if min_number == FFV[8]
            {
                stored_text = "The choice is Starbucks - Nicolson Street"
                self.mapview.setRegion(map_store[8], animated: true)
            }
            else {
                stored_text = "The choice is Kilimanjaro Coffee"
                self.mapview.setRegion(map_store[9], animated: true)
            }
            FFV.removeAll()
            map_store.removeAll()
            combined_text = "You need to study a little bit harder, in my suggestion " + stored_text
            
        }
            
        else if learning_hour<=6 && learning_hour >= 3
        {
            //go to entertainment
            
            let three_sis = CLLocation(latitude: 55.9481798,longitude:-3.192492)
            var d = distanceFromLocation(three_sis)
            FFV.append(d)
            var center = CLLocationCoordinate2D(latitude: three_sis.coordinate.latitude, longitude: three_sis.coordinate.longitude)
            var region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[0]
            
            
            let Irish_bar = CLLocation(latitude: 55.948711,longitude: -3.1935173)
            d = distanceFromLocation(Irish_bar)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: Irish_bar.coordinate.latitude, longitude: Irish_bar.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[1]
            
            
            let doctors = CLLocation(latitude: 55.9455158,longitude: -3.1931222)
            d = distanceFromLocation(doctors)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: doctors.coordinate.latitude, longitude: doctors.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[2]
            
            let black_medicine = CLLocation(latitude:55.9470748,longitude:-3.1859786)
            d = distanceFromLocation(black_medicine)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: black_medicine.coordinate.latitude, longitude: black_medicine.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[3]
            
            let teviot = CLLocation(latitude: 55.9448244,longitude: -3.1879458)
            d = distanceFromLocation(teviot)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: teviot.coordinate.latitude, longitude: teviot.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[4]
            
            let elephant = CLLocation(latitude: 55.9476154,longitude: -3.1916038)
            d = distanceFromLocation(elephant)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: elephant.coordinate.latitude, longitude: elephant.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[5]
            
            let Filmhouse = CLLocation(latitude: 55.9464657,longitude: -3.2082481)
            d = distanceFromLocation(Filmhouse)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: Filmhouse.coordinate.latitude, longitude: Filmhouse.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[6]
            
            let Royal_Mile_Whiskies = CLLocation(latitude: 55.9496,longitude: -3.1939977)
            d = distanceFromLocation(Royal_Mile_Whiskies)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: Royal_Mile_Whiskies.coordinate.latitude, longitude: Royal_Mile_Whiskies.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[7]
            
            let prince_st = CLLocation(latitude: 55.9513343,longitude: -3.2025141)
            d = distanceFromLocation(prince_st)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: prince_st.coordinate.latitude, longitude: prince_st.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[8]
            
            let kilomi_cafe = CLLocation(latitude: 55.9445328,longitude: -3.1857938)
            d = distanceFromLocation(kilomi_cafe)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: kilomi_cafe.coordinate.latitude, longitude: kilomi_cafe.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[9]
            
            min_number = FFV.min()!
            
            //vector V -- Judging elements
            if min_number == FFV[0]
            {
                stored_text = "The choice is Three sister"
                self.mapview.setRegion(map_store[0], animated: true)
            }
            else if min_number == FFV[1]
            {
                stored_text = "The choice is The Irish bar"
                self.mapview.setRegion(map_store[1], animated: true)
            }
            else if min_number == FFV[2]
            {
                stored_text = "The choice is Doctors"
                self.mapview.setRegion(map_store[2], animated: true)
            }
            else if min_number == FFV[3]
            {
                stored_text = "The choice is Black Medicine"
                self.mapview.setRegion(map_store[3], animated: true)
            }
            else if min_number == FFV[4]
            {
                stored_text = "The choice is Teviot"
                self.mapview.setRegion(map_store[4], animated: true)
            }
            else if min_number == FFV[5]
            {
                stored_text = "The choice is Elephant house"
                self.mapview.setRegion(map_store[5], animated: true)
            }
            else if min_number == FFV[6]
            {
                stored_text = "The choice is Filehouse"
                self.mapview.setRegion(map_store[6], animated: true)
            }
            else if min_number == FFV[7]
            {
                stored_text = "The choice is Royal_Mile_Whiskies"
                self.mapview.setRegion(map_store[7], animated: true)
            }
            else if min_number == FFV[8]
            {
                stored_text = "The choice is Prince street"
                self.mapview.setRegion(map_store[8], animated: true)
            }
            else
            {
                stored_text = "The choice is Kilimanjaro Coffee"
                self.mapview.setRegion(map_store[9], animated: true)
            }
            FFV.removeAll()
            map_store.removeAll()
            combined_text = "You need to do something fun, just find a place! In my suggestion, " + stored_text
            
        }
        else
        {
            //go to sport
            let Arthur_seat = CLLocation(latitude: 55.9440825,longitude: -3.1705881)
            var d = distanceFromLocation(Arthur_seat)
            FFV.append(d)
            var center = CLLocationCoordinate2D(latitude: Arthur_seat.coordinate.latitude, longitude: Arthur_seat.coordinate.longitude)
            var region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[0]
            
            let calton_hill = CLLocation(latitude: 55.9552458,longitude: -3.1853297)
            d = distanceFromLocation(calton_hill)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: calton_hill.coordinate.latitude, longitude: calton_hill.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[1]
            
            let sport_center = CLLocation(latitude: 55.9481793,longitude: -3.1832347)
            d = distanceFromLocation(sport_center)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: sport_center.coordinate.latitude, longitude: sport_center.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[2]
            
            let jogging_meadows = CLLocation(latitude: 55.9398623,longitude: -3.1914871)
            d = distanceFromLocation(jogging_meadows)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: jogging_meadows.coordinate.latitude, longitude: jogging_meadows.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[3]
            
            let Basketball = CLLocation(latitude: 55.9481793,longitude: -3.1832347)
            d = distanceFromLocation(Basketball)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: Basketball.coordinate.latitude, longitude: Basketball.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[4]
            
            
            let go_swimming = CLLocation(latitude: 55.948246,longitude: -3.1843335)
            d = distanceFromLocation(go_swimming)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: go_swimming.coordinate.latitude, longitude: go_swimming.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[5]
            
            let go_biking = CLLocation(latitude: 55.9550752,longitude: -3.1565938)
            d = distanceFromLocation(go_biking)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: go_biking.coordinate.latitude, longitude: go_biking.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[6]
            
            let football = CLLocation(latitude: 55.9311167,longitude: -3.1544378)
            d = distanceFromLocation(football)
            FFV.append(d)
            center = CLLocationCoordinate2D(latitude: football.coordinate.latitude, longitude: football.coordinate.longitude)
            region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            map_store.append(region) //[7]
            min_number = FFV.min()!
            
            //vector V -- Judging elements
            if min_number == FFV[0]
            {
                stored_text = "The choice is Arthur' seat"
                self.mapview.setRegion(map_store[0], animated: true)
                
            }
            else if min_number == FFV[1]
            {
                stored_text = "The choice is Calton hill"
                self.mapview.setRegion(map_store[1], animated: true)
                
            }
            else if min_number == FFV[2]
            {
                stored_text = "The choice is The UOE Centre for Sport and Exercise"
                self.mapview.setRegion(map_store[2], animated: true)
                
            }
            else if min_number == FFV[3]
            {
                stored_text = "The choice is Go jogging in Meadows"
                self.mapview.setRegion(map_store[3], animated: true)
                
            }
            else if min_number == FFV[4]
            {
                stored_text = "The choice is Basketball in the gym"
                self.mapview.setRegion(map_store[4], animated: true)
                
            }
            else if min_number == FFV[5]
            {
                stored_text = "The choice is Go swimming in St. Leonard's Land building"
                self.mapview.setRegion(map_store[5], animated: true)
                
            }
            else if min_number == FFV[6]
            {
                stored_text = "The choice is Go biking around the city"
                self.mapview.setRegion(map_store[6], animated: true)
                
            }
            else {
                stored_text = "Football in Peffermill playing fields"
                self.mapview.setRegion(map_store[7], animated: true)
                
            }
            FFV.removeAll()
            map_store.removeAll()
            combined_text = "You are too pressured, try to do some sport! In my suggestion " + stored_text
            
        }
        
        
        
    }
    
    func distanceFromLocation(_ location: CLLocation!)->CLLocationDistance
    {
        let distance = location.distance(from: taskMgr.current_spot)
        return distance
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
