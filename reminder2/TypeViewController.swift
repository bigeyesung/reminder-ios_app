//
//  TypeViewController.swift
//  reminder2
//
//  Created by chenhsi on 2017/1/15.
//  Copyright © 2017年 chenhsi. All rights reserved.
//

import UIKit

class TypeViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!

    @IBOutlet weak var txtTask: UITextField!
    @IBOutlet weak var txtDesc: UITextField!
    @IBOutlet weak var Type_fun_button: UIButton!
    @IBOutlet weak var Type_studying_button: UIButton!
    @IBOutlet weak var Type_exercise_button: UIButton!
    @IBOutlet weak var Fixed_button: UIButton!
    @IBOutlet weak var Flexible_button: UIButton!
    
    let calendar = NSCalendar.current
    var dateFormatter = DateFormatter()
    var day = NSDate()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.txtTask.delegate = self
        self.txtDesc.delegate = self
        
        //Menu button
        if self.revealViewController() != nil
        {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        Type_fun_button.isEnabled = true
        Type_studying_button.isEnabled = true
        Type_exercise_button.isEnabled = true
        Fixed_button.isEnabled = true
        Flexible_button.isEnabled = true

    }

    @IBAction func Fun_button(_ sender: Any)
    {
        taskMgr.addType("fun")
        Type_fun_button.isEnabled = false

    }
    
    @IBAction func Studying_button(_ sender: Any)
    {
        taskMgr.addType("studying")
        print(taskMgr.types)
        Type_studying_button.isEnabled = false

    }
    
    @IBAction func Exercise_button(_ sender: Any)
    {
        taskMgr.addType("exercise")
        Type_exercise_button.isEnabled = false
    }
    
    @IBAction func Fixed_button(_ sender: Any)
    {
        taskMgr.addFixed_or_Flexible(true)
        print(taskMgr.fixed_flexible)
        Fixed_button.isEnabled = false
    }
    
    @IBAction func Flexible_button(_ sender: Any)
    {
        taskMgr.addFixed_or_Flexible(false)
        Flexible_button.isEnabled = false
    }
    
    
    @IBAction func Add_task(_ sender: Any)
    {
        // The last function
        taskMgr.addTask(self.txtTask.text!)
        taskMgr.addFinish_time(txtDesc.text!)
        print(taskMgr.tasks)
        print(taskMgr.hours)
        taskMgr.stored_text = txtTask.text!
        
        // Input user loading vector
        if taskMgr.types == "fun"
        {
            taskMgr.user_loading_vector[2] = taskMgr.user_loading_vector[2] + taskMgr.hours
        }
        else if taskMgr.types == "exercise"
        {
            taskMgr.user_loading_vector[1] = taskMgr.user_loading_vector[1] + taskMgr.hours
        }
        else
        {
            taskMgr.user_loading_vector[0] = taskMgr.user_loading_vector[0] + taskMgr.hours
        }
        
        self.view.endEditing(true)
        txtTask.text = nil
        txtDesc.text = nil
        taskMgr.next_time.append(taskMgr.times[0])
        taskMgr.next_Imp = taskMgr.importances
        Judging_conflict()
        taskMgr.next_time.popLast()
    }
    
    
    

    
    func Judging_conflict()
    {
        
        if (taskMgr.data_store.count == 0)
        {
            // if empty no conflict
            Inputdata()
            pop_three()
            taskMgr.show_values[4] = "No conflict"
            taskMgr.index = 4
        }
        else
        {
            //needs judge
            let pre_num = taskMgr.data_store.count
            for index in 0 ..< pre_num
            {
                
                //Judge only happens in the same day same hour
                
                var OntheSameHour = calendar.isDate(taskMgr.next_time[0], equalTo: taskMgr.data_store[index].agr1, toGranularity: Calendar.Component.hour)
                dateFormatter.dateStyle = DateFormatter.Style.short
                dateFormatter.timeStyle = DateFormatter.Style.short
                var strDate1 = dateFormatter.string(from: taskMgr.next_time[0])
                var strDate2 = dateFormatter.string(from: taskMgr.data_store[index].agr1)
                
                //Judge in the same hour
                if OntheSameHour
                {
                    //previous one can't be changed
                    if taskMgr.data_store[index].agr4==true
                    {
                        taskMgr.show_values[4] = taskMgr.data_store[index].agr5 + " is a fixed task, which cannot be replaced in this time slot"
                        pop_three()
                        taskMgr.index = 4
                        break
                        
                    }
                        // previous one is flexible event
                    else
                    {
                        //Value = Imp + Mot + Hap + Urg
                        let next_value =  taskMgr.motivatations + taskMgr.importances + taskMgr.happiness + taskMgr.urg
                        let pri_value = taskMgr.data_store[index].agr7 + taskMgr.data_store[index].agr8 + taskMgr.data_store[index].agr9 + taskMgr.data_store[index].agr10
                        
                        //Judging 2 values
                        if next_value > pri_value
                        {  //can swap
                            taskMgr.show_values[4] = taskMgr.stored_text + " shows the higher value than " + taskMgr.data_store[index].agr5 + ". In my suggstion, you should do " + taskMgr.stored_text
                            taskMgr.data_store.popLast()
                            Inputdata()
                            pop_three()
                            break
                        }
                            //2 values are the same, jusging from: Urg->Imp->Mot->Hap
                        else if next_value == pri_value
                        {
                            
                            //  Judging urgency factor
                            if taskMgr.urg > taskMgr.data_store[index].agr10
                            {
                                taskMgr.show_values[1] = taskMgr.stored_text + " is more urgent than" + taskMgr.data_store[index].agr5 + ". It's only" + taskMgr.day_left + " day left" + ". In my suggestion, you should do " + taskMgr.stored_text
                                taskMgr.index = 1
                                
                                taskMgr.data_store.popLast()
                                Inputdata()
                                pop_three()
                                break
                            }
                                
                                // Judging importance factor
                            else if taskMgr.importances > taskMgr.data_store[index].agr8
                            {
                                taskMgr.show_values[0] = taskMgr.stored_text + " is more important than " + taskMgr.data_store[index].agr5 + " to you. " + " In my suggestion, you should do " + taskMgr.stored_text
                                taskMgr.index = 0
                                
                                taskMgr.data_store.popLast()
                                Inputdata()
                                pop_three()
                                break
                                
                            }
                                
                                //  Judging motivation factor
                            else if taskMgr.motivatations > taskMgr.data_store[index].agr7
                            {
                                taskMgr.show_values[2] = "The motivation behind it is " + taskMgr.stored_text + " can " + taskMgr.stored_mot + ". In my suggestion, you should do " + taskMgr.stored_text
                                taskMgr.index = 2
                                
                                taskMgr.data_store.popLast()
                                Inputdata()
                                pop_three()
                                break
                            }
                                
                                //  Judging Happiness factor
                            else if taskMgr.happiness > taskMgr.data_store[index].agr9
                            {
                                taskMgr.show_values[3] = taskMgr.stored_text + " can " + "make you feel happier than " + taskMgr.data_store[index].agr5  + ". In my suggestion, why not do " + taskMgr.stored_text + " ?"
                                taskMgr.index = 3
                                
                                taskMgr.data_store.popLast()
                                Inputdata()
                                pop_three()
                                break
                            }
                                
                                // If everything is the same
                            else
                            {
                                taskMgr.show_values[4] = taskMgr.stored_text + " and " + taskMgr.data_store[index].agr5 + " are equally important to you, so in my suggestion, you need to choose yourself."
                                taskMgr.index = 4
                            }
                            
                            
                            
                            
                        }
                            // The next_value is less than the pri_value
                        else
                        {
                            taskMgr.show_values[4] = taskMgr.data_store[index].agr5 + " is more important"
                            pop_three()
                            taskMgr.index = 4
                            break
                            
                        }
                    }
                }
                    // Two events are not in the same hour
                else
                {
                    taskMgr.show_values[4] = "No conflict"
                    taskMgr.index = 4
                    Inputdata()
                    pop_three()
                    break
                }
            }
            
        }
        
        
    }
    
    func pop_three()
    {
        taskMgr.tasks.popLast()
        taskMgr.times.popLast()
        taskMgr.maps.popLast()
    }
    
    func Inputdata()
    {
        taskMgr.data_store.append((taskMgr.times[0], taskMgr.maps[0],taskMgr.types,taskMgr.fixed_flexible, taskMgr.tasks[0],taskMgr.hours, taskMgr.motivatations, taskMgr.importances,taskMgr.happiness,taskMgr.urg))
    }
    //Call when return button pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return false
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
