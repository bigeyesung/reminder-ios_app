//
//  ImpViewController.swift
//  reminder2
//
//  Created by chenhsi on 2017/1/15.
//  Copyright © 2017年 chenhsi. All rights reserved.
//

import UIKit

class ImpViewController: UIViewController {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var Imp_very_button: UIButton!
    @IBOutlet weak var Imp_normal_button: UIButton!
    @IBOutlet weak var Imp_none_button: UIButton!
    @IBOutlet weak var Hap_very_button: UIButton!
    @IBOutlet weak var Hap_normal_button: UIButton!
    @IBOutlet weak var Hap_none_button: UIButton!
    @IBOutlet weak var Mot_learn_button: UIButton!
    @IBOutlet weak var Mot_fit_button: UIButton!
    @IBOutlet weak var Mot_hangout_button: UIButton!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //Menu button
        if self.revealViewController() != nil
        {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        
        Imp_very_button.isEnabled = true
        Imp_normal_button.isEnabled = true
        Imp_none_button.isEnabled = true
        Mot_learn_button.isEnabled = true
        Mot_fit_button.isEnabled = true
        Mot_hangout_button.isEnabled = true
        Hap_very_button.isEnabled = true
        Hap_normal_button.isEnabled = true
        Hap_none_button.isEnabled = true
    }

    @IBAction func Mot_learn(_ sender: Any)
    {
        taskMgr.addMotivation(3)
        print(taskMgr.motivatations)
        taskMgr.stored_mot = "learn the knowledge"
        Mot_learn_button.isEnabled = false
    }
    
    @IBAction func Mot_fit(_ sender: Any)
    {
        taskMgr.addMotivation(2)
        taskMgr.stored_mot = "improve your fitness"
        Mot_fit_button.isEnabled = false
    
    }
    
    @IBAction func Mot_hangout(_ sender: Any)
    {
        taskMgr.addMotivation(1)
        taskMgr.stored_mot = "hangout with your friends"
        Mot_hangout_button.isEnabled = false
    
    }
    
    @IBAction func Hap_very(_ sender: Any)
    {
        taskMgr.addHappiness(3)
        print(taskMgr.happiness)
        Hap_very_button.isEnabled = false

    
    }
    
    @IBAction func Hap_normal(_ sender: Any)
    {
        taskMgr.addHappiness(2)
        Hap_normal_button.isEnabled = false
    
    }
    
    @IBAction func Hap_none(_ sender: Any)
    {
        taskMgr.addHappiness(1)
        Hap_none_button.isEnabled = false
    
    }
    
    @IBAction func Imp_very(_ sender: Any)
    {
        taskMgr.addImportance(3)
        print(taskMgr.importances)
        Imp_very_button.isEnabled = false

    }
    
    @IBAction func Imp_normal(_ sender: Any)
    {
        taskMgr.addImportance(2)
        Imp_normal_button.isEnabled = false
    
    }
    
    @IBAction func Imp_none(_ sender: Any)
    {
        taskMgr.addImportance(1)
        Imp_none_button.isEnabled = false
    
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
