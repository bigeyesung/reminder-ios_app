//
//  TimeViewController.swift
//  reminder2
//
//  Created by chenhsi on 2017/1/15.
//  Copyright © 2017年 chenhsi. All rights reserved.
//

import UIKit
import Foundation

class TimeViewController: UIViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var Continue_button: UIButton!
    @IBOutlet weak var datelabel: UILabel!
    
    @IBOutlet weak var datepicker: UIDatePicker!
    var strDate: String = "None"
    let calendar = Calendar.current
    let Today = Date()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        datelabel.text = ""
        //Menu button
        if self.revealViewController() != nil
        {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        Continue_button.isEnabled = true
        datepicker.addTarget(self, action: #selector(TimeViewController.dataPickerChanged(_:)), for: UIControlEvents.valueChanged)
        
    }
    
    func dataPickerChanged(_ datepicker:UIDatePicker)
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        strDate = dateFormatter.string(from: datepicker.date)
        datelabel.text = strDate
    }
    
    @IBAction func addTime(_ sender: AnyObject)
    {
        taskMgr.addTime(datepicker.date)
        print(taskMgr.times)
        //Urgency factor
        let date1 = calendar.startOfDay(for: Today)
        let date2 = calendar.startOfDay(for: taskMgr.times[0] as Date)
        let difference = (calendar as NSCalendar).components(.day, from: date1, to: date2, options: [])
        taskMgr.day_left = String(describing: difference)
        if difference.day! > 14
        {
            taskMgr.addUrgency(1)
        }
        else if difference.day! <= 14 && difference.day! > 7
        {
            taskMgr.addUrgency(2)
        }
        else
        {
            taskMgr.addUrgency(3)
            print(taskMgr.urg)
        }
        //Urgency factor
        
        Continue_button.isEnabled = false
    
    
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        

        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
