//
//  ScheViewController.swift
//  reminder2
//
//  Created by chenhsi on 2017/1/15.
//  Copyright © 2017年 chenhsi. All rights reserved.
//

import UIKit

class ScheViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var prebutton: UIButton!
    @IBOutlet weak var nextbutton: UIButton!
    @IBOutlet weak var tblTasks: UITableView!
    @IBOutlet weak var time_reveal: UILabel!
    @IBOutlet weak var warning_board: UITextView!
    let calendar = Calendar.current
    var dateFormatter = DateFormatter()
    var day = Date()
    var temp_date = Date() ///temp
    var day_event = [String]()
    var refreshControl = UIRefreshControl()
    var tableviewController = UITableViewController(style: .plain)

    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //Menu button
        if self.revealViewController() != nil
        {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        
        self.tblTasks.reloadData()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        let strDate = dateFormatter.string(from: day)
        time_reveal.text = strDate
        warning_board.text = taskMgr.show_values[taskMgr.index]
        
        // set up refresh control
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(ScheViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tblTasks?.addSubview(refreshControl)
    }

    
    @IBAction func next_day_button(_ sender: Any)
    {
        day = (calendar as NSCalendar).date(byAdding: .day, value: +1, to: day, options: [])!
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        let strDate = dateFormatter.string(from: day)
        time_reveal.text = strDate
        super.viewDidLoad()
        tblTasks.reloadData()

    }
    
    @IBAction func pre_day_button(_ sender: Any)
    {
        day = (calendar as NSCalendar).date(byAdding: .day, value: -1, to: day, options: [])!
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        let strDate = dateFormatter.string(from: day)
        time_reveal.text = strDate
        super.viewDidLoad()
        tblTasks.reloadData()

    }
    
    func refresh(_ sender:AnyObject)
    {
        tblTasks.reloadData()
        refreshControl.endRefreshing()
    }

    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        var day_item:Int = 0
        print("count is ",taskMgr.data_store.count)
        var index = 0
        while(index < taskMgr.data_store.count)
        {
            let date1 = calendar.startOfDay(for: day)
            let date2 = calendar.startOfDay(for: taskMgr.data_store[index].agr1 as Date)
            let difference = (calendar as NSCalendar).components(.day, from: date1, to: date2, options: [])
            
            if difference.day == 0
            {
                day_item = day_item + 1
                day_event.append(taskMgr.data_store[index].agr5)
            }
            index += 1;
        }
        return day_item
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "Default Tasks")
        
        cell.textLabel?.text = taskMgr.data_store[(indexPath as NSIndexPath).row].agr5
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        let strDate = dateFormatter.string(from: taskMgr.data_store[(indexPath as NSIndexPath).row].agr1 as Date)
        cell.detailTextLabel!.text = strDate
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
