First, every data structure is stored in TaskManager.swift. Other classes are used for presenting different pages.

Main.storyboard is used for the user interface.
OurController.swift is used for presenting the main page.
TimeViewController.swift is used for choosing the date, which includes date, hour and minute.
TypeViewController.swift is used for choosing the type page.
PlaceViewController.swift is used for showing the location page.
ImpViewController.swift is used for the decision factors page.
SpareViewController.swift is used for the spare time function page.
ScheViewController.swift is used for the schedule page.